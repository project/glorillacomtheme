<?php phptemplate_comment_wrapper(NULL, $node->type); ?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
<?php print $picture ?>
<div style="float: right;"><table class="contentpaneopen"><tbody><tr>
	<td class="buttonheading" align="right"><a href="/node/<?php print $node->nid; ?>/pdf" title="PDF"><img src="/sites/all/themes/glorillacomtheme/pdf_button.png" alt="PDF"></a></td>
	<td class="buttonheading" align="right"><a href="/print/<?php print $node->nid; ?>" title="Print"><img src="/sites/all/themes/glorillacomtheme/printButton.png" alt="Print"></a></td>
	<td class="buttonheading" align="right"><a href="/send/send/<?php print $node->nid; ?>" title="E-mail"><img src="/sites/all/themes/glorillacomtheme/emailButton.png" alt="E-mail"></a></td>
</tr></tbody></table></div>
<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <?php if ($submitted): ?>
    <div class="small"><?php print t('Written by !username', array('!username' => theme('username', $node), '!date' => format_date($node->created, 'custom', 'l, d F Y H:i'))); ?> <?php if ($taxonomy): ?>in<?php print $terms ?><?php endif;?></div>
    <div class="small"><?php print t('!date', array('!username' => theme('username', $node), '!date' => format_date($node->created, 'custom', 'l, d F Y H:i'))); ?></div>
  <?php endif; ?>

  <div class="content">
    <?php print $content ?>
  </div>

  <?php if ($submitted): ?>
    <div class="small"><?php print t('Last Updated (!date)', array('!username' => theme('username', $node), '!date' => format_date($node->changed, 'custom', 'l, d F Y H:i'))); ?></div>
  <?php endif; ?>

<!--  <div class="clear-block clear">
    <div class="meta">
    <?php if ($taxonomy): ?>
      <div class="terms"><?php print $terms ?></div>
    <?php endif;?>
    </div>
-->
    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
  </div>

</div>