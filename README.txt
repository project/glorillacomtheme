This theme is a port of the CMS Joomla. This theme was originally
designed by Joomla.org (http://www.joomla.org/).

The GLORilla.com theme is a two or three column theme.

You can see a demo at http://www.glorilla.com/

The GLORilla.com theme is currently only supported for Drupal 5.x, however
we will be releasing a version for 6.x as soon as we have the time to
make the appropriate changes.

If you have any comments or suggestions, write that to us at
http://www.glorilla.com/forum/10

This theme was developed by http://www.fireflystream.com/

