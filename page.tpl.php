<?php
$topregionblocks = 0;
if($header || $header2) $topregionblocks = 1;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML lang=<?php print $language ?> xml:lang="<?php print $language ?>" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
<LINK href="<?php print base_path() . path_to_theme() ?>/sites/all/themes/glorillacomtheme/images/favicon.ico" type=image/x-icon rel="shortcut icon">
<LINK href="<?php print base_path() . path_to_theme() ?>/css/jsystem.css" type=text/css rel=stylesheet>
<LINK href="<?php print base_path() . path_to_theme() ?>/css/general.css" type=text/css rel=stylesheet>
<LINK href="<?php print base_path() . path_to_theme() ?>/css/template.css" type=text/css rel=stylesheet>
<LINK href="<?php print base_path() . path_to_theme() ?>/css/blue.css" type=text/css rel=stylesheet>
<LINK href="<?php print base_path() . path_to_theme() ?>/css/blue_bg.css" type=text/css rel=stylesheet>

<!--[if lte IE 6]>
<LINK href="<?php print base_path() . path_to_theme() ?>/css/ieonly.css" type=text/css rel=stylesheet>
<![endif]-->
   </head>

<BODY <?php print phptemplate_body_class($sidebar_left, $sidebar_right); ?> class="color_blue bg_blue width_fmax" id=page_bg>
<A id=up name=up></A>
<DIV class=center align=center>
<DIV id=wrapper>
<DIV id=wrapper_r>
<DIV id=header>
<DIV id=header_l>
<DIV id=header_r>
<DIV id=logo><?php
          // Prepare header
          $site_fields = array();
          if ($site_name) {
            $site_fields[] = check_plain($site_name);
          }
          if ($site_slogan) {
            $site_fields[] = check_plain($site_slogan);
          }
          $site_title = implode(' ', $site_fields); 
          $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
          $site_html = implode(' ', $site_fields);

          if ($logo || $site_title) {
            if ($logo) {
              print '<a href="'. check_url($base_path) .'" title="'.$site_title .'">'.'<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
            }
            print '</a></h1>';
          }
        ?>
</DIV>
<TABLE class=contentpaneopen>
  <TBODY>
  <TR>
    <TD vAlign=top colSpan=2>
<p><?php if ($logo || $site_title) {
            print '<h1><a href="'. check_url($base_path) .'" title="'.$site_title .'">'.$site_name.'</a>&nbsp;&nbsp;|&nbsp;&nbsp;' . $site_slogan . '</h1>';
};?>
</p>
<P><?php if ($mission): print '<div id="mission">'. $mission .'</div>';endif; ?></P>
      <P><?php if (isset($secondary_links)) : ?>
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
        <?php endif; ?></P>
    </TD></TR></TBODY></TABLE></DIV></DIV></DIV>
<DIV id=tabarea>
<DIV id=tabarea_l>
<DIV id=tabarea_r>
<DIV id=tabmenu>
<TABLE class=pill cellSpacing=0 cellPadding=0>
  <TBODY>
  <TR>
    <TD class=pill_l>&nbsp;</TD>
    <TD class=pill_m>
      <DIV id=pillmenu>
      <UL id=mainlevel-nav>
        <LI>
        <?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
        <?php endif; ?>
        </LI>
      </UL>
      </DIV>
    </TD>
    <TD class=pill_r>&nbsp;</TD>
</TR></TBODY></TABLE>
</DIV></DIV></DIV></DIV>                                             
<?php if ($search_box): ?><?php print $search_box ?><?php endif; ?>
<SPAN class="breadcrumbs"><?php if ($breadcrumb): print $breadcrumb; endif; ?></SPAN> 
<DIV class=clr></DIV>
<DIV id=whitebox>
<DIV id=whitebox_t>
<DIV id=whitebox_tl>
<DIV id=whitebox_tr></DIV></DIV></DIV>
<DIV id=whitebox_m>
<DIV id=area>
<DIV id=leftcolumn>
      <?php if ($sidebar_left): ?>
          <?php print $sidebar_left ?>
      <?php endif; ?>
</DIV>

<DIV id=maincolumn>
<?php if ($topregionblocks):?>
<TABLE class="nopad user1user2">
  <TBODY>
  <TR vAlign=top>
    <TD>
      <DIV class=moduletable>  
         <div id="header-region" class="clear-block"><?php print $header; ?></div>
      </DIV>
    </TD>
<?php if($header2):?><TD class=greyline>&nbsp;</TD>
    <TD>
      <DIV class=moduletable>  
         <div id="header-region2" class="clear-block"><?php print $header2; ?></div>
      </DIV>
    </TD><?php endif; ?>
</TR></TBODY></TABLE>
<DIV id=maindivider></DIV>
<?php endif; ?>
 
<TABLE class=nopad>
  <TBODY>
  <TR vAlign=top>
    <TD>
          <?php if ($title): print '<h2 class="componentheading"'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
          <?php if ($tabs): print $tabs .'</div>'; endif; ?>

          <?php if (isset($tabs2)): print $tabs2; endif; ?>

          <?php if ($help): print $help; endif; ?>
          <?php if ($messages): print $messages; endif; ?>
          <?php print $content ?>
          <span class="clear"></span>
      <DIV class=moduletable>
      <DIV class=bannergroup>
      </DIV></DIV></DIV>
<div><?php print $footer_message ?></div>      
</TD>

<?php if ($sidebar_right): ?>
    <div id="sidebar-right" class="sidebar">
    <TD class=greyline>&nbsp;</TD>
    <TD width=170>
          <?php if (!$sidebar_left && $search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>
          <?php print $sidebar_right ?>
    </TD>
    </div>
<?php endif; ?>
</TR></TBODY></TABLE></DIV>
<DIV class=clr></DIV></DIV>
<DIV class=clr></DIV></DIV>
<DIV id=whitebox_b>
<DIV id=whitebox_bl>
<DIV id=whitebox_br></DIV></DIV></DIV></DIV>
<DIV id=footerspacer></DIV></DIV>
<DIV id=footer>
<DIV id=footer_l>
<DIV id=footer_r>
<P id=syndicate><?php print $feed_icons ?><?php if ($feed_icons): ?> <A href="/rss.xml"><SPAN>Feed Entries</SPAN></A><?php endif; ?></P>
<P id=power_by>Powered by <A href="http://www.glorilla.com/">GLORilla.com</A> 
</P></DIV></DIV></DIV></DIV></DIV>
<?php print $closure ?></BODY></HTML>
